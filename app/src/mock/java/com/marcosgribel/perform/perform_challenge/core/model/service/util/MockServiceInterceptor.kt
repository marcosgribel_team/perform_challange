package com.marcosgribel.perform.perform_challenge.core.model.service.util

import android.os.SystemClock
import okhttp3.*
import timber.log.Timber
import java.io.InputStream
import java.io.InputStreamReader

/**
 * Created by marcosgribel on 11/13/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class MockServiceInterceptor : Interceptor {


    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        var xmlFile = getXmlFile(request)
        var xml = readXmlFile(xmlFile)

        // set delay to requisition
        SystemClock.sleep(2000)

        return Response.Builder()
                .message(xml.toString())
                .body(ResponseBody.create(MediaType.parse("text/xml"), xml.toString()))
                .request(request)
                .protocol(Protocol.HTTP_1_1)
                .code(200)
                .build()

    }

    private fun getXmlFile(request: Request): String {
        var method = request.method()

        var path = request.url().encodedPath()
        var builder = StringBuilder()

        builder.append(method.toLowerCase())
                .append(path)

        return builder
                .toString()
                .replace("/", "_")

    }

    private fun readXmlFile(xmlFileName: String): StringBuilder {
        Timber.d("File Name: " + xmlFileName)
        var builder = StringBuilder()

        var input: InputStream? = RetrofitServiceConnector::class.java.classLoader.getResourceAsStream(xmlFileName) ?: throw NullPointerException("Check you local respository! File name: " + xmlFileName)

        var isr = InputStreamReader(input)

        for (line in isr.readLines()) {
            builder.append(line)
        }

        isr.close()
        input?.close()

        return builder
    }

}