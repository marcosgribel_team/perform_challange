package com.marcosgribel.perform.perform_challenge.core.model.service.util

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/10/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
open class RetrofitServiceConnector @Inject constructor(builder: Retrofit.Builder,  okHttpClient: OkHttpClient.Builder) : BaseServiceConnector(builder, okHttpClient) {

    init {
        okHttpClient.interceptors().add(MockServiceInterceptor())
    }

    override fun <S> request(url: String, service: Class<S>): S {
        return builder
                .baseUrl(url)
                .client(okHttpClient.build())
                .build()
                .create(service)
    }

}