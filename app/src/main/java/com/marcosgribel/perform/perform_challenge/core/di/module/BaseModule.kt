package com.marcosgribel.perform.perform_challenge.core.di.module

import com.marcosgribel.perform.perform_challenge.core.AppScope
import com.marcosgribel.perform.perform_challenge.core.model.service.BaseService
import com.marcosgribel.perform.perform_challenge.core.model.service.BaseServiceImpl
import com.marcosgribel.perform.perform_challenge.core.model.usecase.BaseUseCase
import com.marcosgribel.perform.perform_challenge.core.model.usecase.BaseUseCaseImpl
import com.marcosgribel.perform.perform_challenge.core.presentation.presenter.BasePresenter
import com.marcosgribel.perform.perform_challenge.core.presentation.presenter.BasePresenterImpl
import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class BaseModule(var baseView: BaseView) {

    @Provides
    @AppScope
    fun provideActivityView(): BaseView.Activity {
        return baseView as BaseView.Activity
    }


    @Provides
    @AppScope
    fun provideBasePresenter(basePresenter: BasePresenterImpl): BasePresenter {
        return basePresenter
    }


    @Provides
    @Singleton
    fun provideBaseService(baseService: BaseServiceImpl): BaseService {
        return baseService
    }

    @Provides
    @Singleton
    fun provideBaseUseCase(baseUseCase: BaseUseCaseImpl): BaseUseCase {
        return baseUseCase
    }

}