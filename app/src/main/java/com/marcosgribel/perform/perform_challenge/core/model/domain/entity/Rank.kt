package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Root(name = "ranking", strict = false)
data class Rank constructor(
        @field:Attribute(name = "rank", required = false)
        var rank: Int? = null,
        @field:Attribute(name = "last_rank", required = false)
        var lastRank: Int? = null,
        @field:Attribute(name = "zone_start", required = false)
        var zoneStart: String? = null,
        @field:Attribute(name = "zone_end", required = false)
        var zoneEnd: String? = null,
        @field:Attribute(name = "team_id", required = false)
        var teamId: Long? = null,
        @field:Attribute(name = "club_name", required = false)
        var clubName: String? = null,
        @field:Attribute(name = "countrycode", required = false)
        var countryCode: String? = null,
        @field:Attribute(name = "area_id", required = false)
        var areaId: Long? = null,
        @field:Attribute(name = "matches_total", required = false)
        var matchesTotal: Int? = null,
        @field:Attribute(name = "matches_won", required = false)
        var matchesWon: Int? = null,
        @field:Attribute(name = "matches_draw", required = false)
        var matchesDraw: Int? = null,
        @field:Attribute(name = "matches_lost", required = false)
        var matchesLost: Int? = null,
        @field:Attribute(name = "goals_pro", required = false)
        var goalsPro: Int? = null,
        @field:Attribute(name = "goals_against", required = false)
        var goalsAgainst: Int? = null,
        @field:Attribute(name = "points", required = false)
        var poits: Int? = null


)
