package com.marcosgribel.perform.perform_challenge.home.presentation.presenter

import com.marcosgribel.perform.perform_challenge.core.presentation.presenter.BasePresenterImpl
import com.marcosgribel.perform.perform_challenge.home.presentation.view.ui.activity.HomeView
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class HomeActivityPresenterImpl @Inject constructor(val view: HomeView): BasePresenterImpl(view), HomePresenter.Activity {

}