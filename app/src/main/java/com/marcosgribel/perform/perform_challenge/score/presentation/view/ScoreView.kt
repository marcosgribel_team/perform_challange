package com.marcosgribel.perform.perform_challenge.score.presentation.view

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Competition
import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Group
import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface ScoreView : BaseView {

    interface Fragment : ScoreView {

        fun setHeader(competition: Competition?)

        fun setContent(items: List<Any>?)

    }
}