package com.marcosgribel.perform.perform_challenge.core

import javax.inject.Scope
import javax.inject.Singleton

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Scope
@MustBeDocumented
@kotlin.annotation.Retention(value = AnnotationRetention.RUNTIME)
annotation class AppScope