package com.marcosgribel.perform.perform_challenge.core.di.component

import com.marcosgribel.perform.perform_challenge.core.di.App
import com.marcosgribel.perform.perform_challenge.core.di.module.AppModule
import com.marcosgribel.perform.perform_challenge.home.di.component.HomeComponent
import com.marcosgribel.perform.perform_challenge.home.di.module.HomeModule
import com.marcosgribel.perform.perform_challenge.news.di.NewsComponent
import com.marcosgribel.perform.perform_challenge.news.di.NewsModule
import com.marcosgribel.perform.perform_challenge.rank.model.di.RankComponent
import com.marcosgribel.perform.perform_challenge.rank.model.di.RankModule
import com.marcosgribel.perform.perform_challenge.score.di.ScoreComponent
import com.marcosgribel.perform.perform_challenge.score.di.ScoreModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by marcosgribel on 11/10/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun inject(app: App)

    fun plus(module: HomeModule): HomeComponent

    fun plus(module: NewsModule): NewsComponent

    fun plus(module: ScoreModule): ScoreComponent

    fun plus(module: RankModule): RankComponent

}