package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root
import java.util.*

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Root(strict = false)
data class Season constructor(
        @field:Attribute(name = "season_id", required = false)
        var id: Long? = null,
        @field:Attribute(name = "name", required = false)
        var name: String? = null,
        @field:Attribute(name = "start_date", required = false)
        var startDate: Date? = null,
        @field:Attribute(name = "end_date", required = false)
        var endDate: Date? = null,
        @field:Element(name="round", required = false)
        var round: Round? = null
)