package com.marcosgribel.perform.perform_challenge.news.model.service

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Rss
import com.marcosgribel.perform.perform_challenge.core.model.service.BaseService
import io.reactivex.Observable


/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface NewsService : BaseService {

    fun getLatest(): Observable<Rss>

}