package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Root(strict = false)
class Group constructor(
        @field:Attribute(name="group_id", required = false)
        var id: Long? = null,
        @field:Attribute(name = "name", required = false)
        var name: String? = null,
        @field:ElementList(name = "match", inline = true)
        var matches: List<Match>? = null
)