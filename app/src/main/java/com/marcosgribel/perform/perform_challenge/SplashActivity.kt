package com.marcosgribel.perform.perform_challenge

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.marcosgribel.perform.perform_challenge.home.presentation.view.ui.activity.HomeActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        shimmer_frame_layout.startShimmerAnimation()

        Observable.just(true)
                .delay(5, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    startActivity(HomeActivity.newInstance(this))
                }

    }
}
