package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root
import java.util.*

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Root(strict = false)
data class Match constructor(
        @field:Attribute(name = "match_id", required = false)
        var id: Long? = null,
        @field:Attribute(name = "team_A_name", required = false)
        var teamAName: String? = null,
        @field:Attribute(name = "team_B_name", required = false)
        var teamBName: String? = null,
        @field:Attribute(name = "fs_A", required = false)
        var fsA: Int? = null,
        @field:Attribute(name = "fs_B", required = false)
        var fsB: Int? = null
)