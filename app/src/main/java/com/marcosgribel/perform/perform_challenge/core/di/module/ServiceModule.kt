package com.marcosgribel.perform.perform_challenge.core.di.module

import com.marcosgribel.perform.perform_challenge.news.model.service.NewsService
import com.marcosgribel.perform.perform_challenge.news.model.service.NewsServiceImpl
import com.marcosgribel.perform.perform_challenge.rank.model.service.RankService
import com.marcosgribel.perform.perform_challenge.rank.model.service.RankServiceImpl
import com.marcosgribel.perform.perform_challenge.score.model.service.ScoreService
import com.marcosgribel.perform.perform_challenge.score.model.service.ScoreServiceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class ServiceModule {

    @Provides
    @Singleton
    fun provideNewsService(service: NewsServiceImpl): NewsService {
        return service
    }

    @Provides
    @Singleton
    fun provideScoreService(service: ScoreServiceImpl): ScoreService {
        return service
    }

    @Provides
    @Singleton
    fun provideRankService(service: RankServiceImpl): RankService {
        return service
    }


}