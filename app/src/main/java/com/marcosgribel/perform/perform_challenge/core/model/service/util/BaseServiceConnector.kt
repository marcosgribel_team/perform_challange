package com.marcosgribel.perform.perform_challenge.core.model.service.util

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/10/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
open class BaseServiceConnector @Inject constructor(protected var builder: Retrofit.Builder, protected var okHttpClient: OkHttpClient.Builder) {

    /*

        Method that is responsible to execute all requests by retrofit

     */
    open fun <S> request(url: String, service: Class<S>): S {
        return builder
                .baseUrl(url)
                .client(okHttpClient.build())
                .build()
                .create(service)
    }

}