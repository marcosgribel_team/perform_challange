package com.marcosgribel.perform.perform_challenge.score.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.marcosgribel.perform.perform_challenge.R
import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Group
import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Match
import com.marcosgribel.perform.perform_challenge.core.presentation.adapter.BaseRecyclerViewAdapter

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class ScoreRecyclerViewAdapter constructor(val mContext: Context) : BaseRecyclerViewAdapter<Any>(mContext) {

    private val HEADER_TYPE = 0
    private val ITEM_TYPE = 1

    inner class ScoreHeaderViewHolder(private val mView: View?) : BaseViewHolder(mView) {
        var txtHeader = mView?.findViewById<TextView>(R.id.txt_header_fragment_score_list)
    }

    inner class ScoreItemViewHolder(private val mView: View?) : BaseViewHolder(mView) {
        var txtTeamNameA = mView?.findViewById<TextView>(R.id.txt_name_team_a)
        var txtTeamNameB = mView?.findViewById<TextView>(R.id.txt_name_team_b)
        var txtValueOfScore = mView?.findViewById<TextView>(R.id.txt_value_of_score)
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BaseViewHolder {


        return when (viewType) {
            HEADER_TYPE -> {
                val view = LayoutInflater.from(parent?.context).inflate(R.layout.header_fragment_score_list, parent, false)
                ScoreHeaderViewHolder(view)
            }
            ITEM_TYPE -> {
                val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_fragment_score_list, parent, false)
                ScoreItemViewHolder(view)
            }
            else -> {
                throw RuntimeException("View holder unavailable!")
            }
        }


    }

    override fun onBindViewHolder(holder: BaseViewHolder?, position: Int) {
        val item = mItems[position]

        if (getItemViewType(position) == HEADER_TYPE) {
            holder as ScoreHeaderViewHolder
            val group = item as Group
            holder.txtHeader?.text = group.name

        } else if (getItemViewType(position) == ITEM_TYPE) {
            holder as ScoreItemViewHolder
            val match = item as Match
            holder.txtTeamNameA?.text = match.teamAName
            holder.txtTeamNameB?.text = match.teamBName
            holder.txtValueOfScore?.text = String.format("%d - %d", match.fsA, match.fsB)
        }

    }

    override fun getItemViewType(position: Int): Int {
        super.getItemViewType(position)
        val item = mItems[position]

        return if (item is Group)
            HEADER_TYPE
        else
            ITEM_TYPE

    }


}