package com.marcosgribel.perform.perform_challenge.rank.model.di

import com.marcosgribel.perform.perform_challenge.core.AppScope
import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView
import com.marcosgribel.perform.perform_challenge.rank.presentation.presenter.RankListFragmentPresenterImpl
import com.marcosgribel.perform.perform_challenge.rank.presentation.presenter.RankPresenter
import com.marcosgribel.perform.perform_challenge.rank.presentation.view.RankView
import dagger.Module
import dagger.Provides

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class RankModule constructor(val mView: BaseView) {


    @Provides
    @AppScope
    fun provideRankView(): RankView {
        return mView as RankView
    }


    @Provides
    @AppScope
    fun provideRankFragmentView(): RankView.Fragment {
        return mView as RankView.Fragment
    }


    @Provides
    @AppScope
    fun provideRankListFragmentPresenter(presenter: RankListFragmentPresenterImpl): RankPresenter.Fragment {
        return presenter
    }


}