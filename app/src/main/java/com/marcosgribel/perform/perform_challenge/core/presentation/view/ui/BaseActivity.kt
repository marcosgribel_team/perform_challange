package com.marcosgribel.perform.perform_challenge.core.presentation.view.ui

import android.support.v7.app.AppCompatActivity
import android.view.View
import com.marcosgribel.perform.perform_challenge.core.presentation.presenter.BasePresenter
import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView
import kotlinx.android.synthetic.main.progress_bar.*
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
open class BaseActivity<V : BasePresenter> : AppCompatActivity(), BaseView.Activity {


    @Inject protected lateinit var basePresenter: V

    override fun onStart() {
        super.onStart()
        onAttach()
    }

    override fun onStop() {
        onDetach()
        super.onStop()
    }

    override fun onAttach() {
        basePresenter.attach()
    }

    override fun onDetach() {
        basePresenter.detach()
    }


    override fun isToShowProgressBar(value: Boolean) {
        showProgressBar(value)
    }
}

/*
    Extension to show progress bar
 */
fun AppCompatActivity.showProgressBar(value: Boolean){
    if (value) {
        progress_bar?.visibility = View.VISIBLE
    } else {
        progress_bar?.visibility = View.GONE
    }
}