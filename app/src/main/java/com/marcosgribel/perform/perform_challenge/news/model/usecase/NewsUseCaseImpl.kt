package com.marcosgribel.perform.perform_challenge.news.model.usecase

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Rss
import com.marcosgribel.perform.perform_challenge.core.model.usecase.BaseUseCaseImpl
import com.marcosgribel.perform.perform_challenge.news.model.service.NewsService
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class NewsUseCaseImpl @Inject constructor(private val service: NewsService) : BaseUseCaseImpl(service), NewsUseCase  {

    override fun getLatest(): Observable<Rss> {
        return service.getLatest()
    }

}