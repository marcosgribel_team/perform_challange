package com.marcosgribel.perform.perform_challenge.score.di

import com.marcosgribel.perform.perform_challenge.core.AppScope
import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView
import com.marcosgribel.perform.perform_challenge.score.presentation.presenter.ScoreListFragmentPresenterImpl
import com.marcosgribel.perform.perform_challenge.score.presentation.presenter.ScorePresenter
import com.marcosgribel.perform.perform_challenge.score.presentation.view.ScoreView
import dagger.Module
import dagger.Provides

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class ScoreModule (val mView: BaseView) {

    @Provides
    @AppScope
    fun provideScoreView() : ScoreView {
        return mView as ScoreView
    }


    @Provides
    @AppScope
    fun provideScoreFragmentView() : ScoreView.Fragment {
        return mView as ScoreView.Fragment
    }


    @Provides
    @AppScope
    fun provideScoreListFragmentPresenter(presenter: ScoreListFragmentPresenterImpl) : ScorePresenter.Fragment {
        return presenter
    }

}