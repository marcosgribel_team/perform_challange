package com.marcosgribel.perform.perform_challenge.core.presentation.presenter

import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
open class BasePresenterImpl @Inject constructor(baseView: BaseView) : BasePresenter {

    protected var mCompositeDisposable = CompositeDisposable()


    override fun attach() {

    }

    override fun detach() {
        mCompositeDisposable.clear()
    }

}