package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.simpleframework.xml.*

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
data class Item constructor(
        @field:Element(name = "guid", required = false)
        var guid: String? = null,
        @field:Element(name = "pubDate", required = false)
        var pubDate: String? = null,
        @field:Element(name = "title", required = false)
        var title: String? = null,
        @field:Element(name = "category", required = false)
        var category: String? = null,
        @field:Element(name = "enclosure", required = false)
        var enclosure: Enclosure? = null,
        @field:Element(name = "description", required = false)
        var description: String? = null,
        @field:Element(name = "link", required = false)
        var link: String? = null
)