package com.marcosgribel.perform.perform_challenge.home.presentation.view.ui.activity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import com.marcosgribel.perform.perform_challenge.R
import com.marcosgribel.perform.perform_challenge.core.di.App
import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Item
import com.marcosgribel.perform.perform_challenge.core.presentation.view.ui.BaseActivity
import com.marcosgribel.perform.perform_challenge.core.presentation.view.ui.BaseFragment
import com.marcosgribel.perform.perform_challenge.home.di.module.HomeModule
import com.marcosgribel.perform.perform_challenge.home.presentation.presenter.HomePresenter
import com.marcosgribel.perform.perform_challenge.news.presentation.view.fragment.NewsListFragment
import com.marcosgribel.perform.perform_challenge.rank.presentation.view.ui.fragment.RankListFragment
import com.marcosgribel.perform.perform_challenge.score.presentation.view.fragment.ScoreListFragment
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*


class HomeActivity : BaseActivity<HomePresenter.Activity>(), HomeView.Activity, NavigationView.OnNavigationItemSelectedListener, BaseFragment.OnListFragmentInteractionListener {

    companion object {

        fun newInstance(context: Context): Intent {
            var intent = Intent(context, HomeActivity::class.java)

            return intent
        }

    }


    var mMenuItemSelected: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        App.get(this)
                .getAppComponent()
                .plus(HomeModule(this))
                .inject(this)


        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        render(R.id.nav_news)

    }


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        render(item.itemId)

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


    /*
        Render fragment according selected item from menu
     */
    override fun render(itemId: Int) {
        mMenuItemSelected = itemId

        var fragmentTransaction = supportFragmentManager.beginTransaction()

        var fragment: Fragment? = null
        var tag: String? = null
        when (itemId) {
            R.id.nav_news -> {
                fragment = NewsListFragment.newInstance()
                tag = NewsListFragment.TAG
            }
            R.id.nav_scores -> {
                fragment = ScoreListFragment.newInstance()
                tag = ScoreListFragment.TAG
            }
            R.id.nav_standings -> {
                fragment = RankListFragment.newInstance()
                tag = RankListFragment.TAG
            }

        }

        fragmentTransaction
                .replace(R.id.content_home_activity, fragment, tag)
                .addToBackStack(null)
                .commit()

    }


    override fun <E> onListFragmentInteraction(item: E) {
        when (mMenuItemSelected) {
            R.id.nav_news -> {
                onItemNewsClickListener(item as Item)
            }
        }
    }


    /*

     */
   private fun onItemNewsClickListener( item: Item) {
        var customTabsIntent = CustomTabsIntent.Builder()
                .setToolbarColor(ContextCompat.getColor(this, R.color.primary))
                .setShowTitle(true)
                .build()

        customTabsIntent.launchUrl(this, Uri.parse(item.link?.trim()))
    }


}
