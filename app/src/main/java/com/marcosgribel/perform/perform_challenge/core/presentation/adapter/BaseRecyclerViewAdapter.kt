package com.marcosgribel.perform.perform_challenge.core.presentation.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import com.marcosgribel.perform.perform_challenge.core.presentation.view.ui.BaseFragment

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
abstract class BaseRecyclerViewAdapter<E : Any> constructor(val context: Context, val mListener: BaseFragment.OnListFragmentInteractionListener?) : RecyclerView.Adapter<BaseRecyclerViewAdapter.BaseViewHolder>() {
    constructor(context: Context) : this(context, null)

    /*

     */
    open class BaseViewHolder(val mBaseView: View?) : RecyclerView.ViewHolder(mBaseView)


    var mItems: ArrayList<E> = ArrayList<E>()


    fun onBindView(holder: BaseViewHolder?, position: Int) {
        if (holder is BaseViewHolder) {
            holder.mBaseView?.setOnClickListener {
                mListener?.onListFragmentInteraction(mItems[position])
            }
        }

    }


    override fun getItemCount(): Int {
        return mItems.count()
    }

    fun clear(){
        mItems.clear()
        notifyDataSetChanged()
    }

    fun addAll(items: List<E>){
        mItems.addAll(items)
        notifyDataSetChanged()
    }


}