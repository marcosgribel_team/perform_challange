package com.marcosgribel.perform.perform_challenge.home.presentation.view.ui.activity

import android.view.MenuItem
import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface HomeView : BaseView {

    interface Activity : HomeView {

        fun render(itemId: Int)

    }
}