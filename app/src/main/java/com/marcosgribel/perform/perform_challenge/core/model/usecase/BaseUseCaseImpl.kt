package com.marcosgribel.perform.perform_challenge.core.model.usecase

import com.marcosgribel.perform.perform_challenge.core.model.service.BaseService
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
open class BaseUseCaseImpl @Inject constructor(baseService: BaseService) : BaseUseCase {

}