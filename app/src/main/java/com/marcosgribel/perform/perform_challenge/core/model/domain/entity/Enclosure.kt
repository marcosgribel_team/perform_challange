package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */

data class Enclosure constructor(
        @field:Attribute(name = "length", required = false)
        var length: String? = null,
        @field:Attribute(name = "type", required = false)
        var type: String? = null,
        @field:Attribute(name = "url", required = false)
        var url: String? = null
)


