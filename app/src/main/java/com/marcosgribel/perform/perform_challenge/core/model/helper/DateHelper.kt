package com.marcosgribel.perform.perform_challenge.core.model.helper

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class DateHelper {

    enum class Pattern (val pattern: String) {
        EEE_dd_MMM_yyyy_hh_mm_ss_z("EEE, dd MMM yyyy HH:mm:ss z"),
        EEEEE_dd_MMMMM_yyyy_hh_mm("E, dd MM yyyy - hh:mm"),
    }


    companion object {

        fun formartStringDate(value: String?, inputPattern: String, outPutPattern: String): String {

            if(value == null) return ""

            val readFormat = SimpleDateFormat(inputPattern, Locale.US)
            val writeFormat = SimpleDateFormat(outPutPattern, Locale.US)

            val date = readFormat.parse(value)
            return writeFormat.format(date)
        }

    }

}