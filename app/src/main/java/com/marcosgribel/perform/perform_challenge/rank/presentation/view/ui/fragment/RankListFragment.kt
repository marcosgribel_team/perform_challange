package com.marcosgribel.perform.perform_challenge.rank.presentation.view.ui.fragment


import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcosgribel.perform.perform_challenge.R
import com.marcosgribel.perform.perform_challenge.core.di.App
import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Rank
import com.marcosgribel.perform.perform_challenge.core.presentation.view.ui.BaseFragment
import com.marcosgribel.perform.perform_challenge.rank.model.di.RankModule
import com.marcosgribel.perform.perform_challenge.rank.presentation.adapter.RankRecyclerViewAdapter
import com.marcosgribel.perform.perform_challenge.rank.presentation.presenter.RankPresenter
import com.marcosgribel.perform.perform_challenge.rank.presentation.view.RankView


/**
 * A simple [Fragment] subclass.
 */
class RankListFragment : BaseFragment<RankPresenter.Fragment>(), RankView.Fragment {


    companion object {
        val TAG = RankListFragment::class.java.simpleName.toString()

        fun newInstance(): Fragment {
            return RankListFragment()
        }
    }

    lateinit var mAdapter: RankRecyclerViewAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        App.get(context)
                .getAppComponent()
                .plus(RankModule(this))
                .inject(this)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        val view = inflater!!.inflate(R.layout.fragment_rank_list, container, false)

        mAdapter = RankRecyclerViewAdapter(context)

        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view_of_ranks)
        recyclerView.setHasFixedSize(true)

        recyclerView.adapter = mAdapter

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        basePresenter.loadData()

    }


    override fun setContent(items: List<Rank>?) {
        if (items == null) return

        mAdapter.clear()
        mAdapter.addAll(items)
    }

}// Required empty public constructor
