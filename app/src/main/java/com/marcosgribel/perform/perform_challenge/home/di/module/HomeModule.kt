package com.marcosgribel.perform.perform_challenge.home.di.module

import com.marcosgribel.perform.perform_challenge.core.AppScope
import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView
import com.marcosgribel.perform.perform_challenge.home.presentation.presenter.HomeActivityPresenterImpl
import com.marcosgribel.perform.perform_challenge.home.presentation.presenter.HomePresenter
import com.marcosgribel.perform.perform_challenge.home.presentation.view.ui.activity.HomeView
import dagger.Module
import dagger.Provides

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class HomeModule(private val view: BaseView) {


    @Provides
    @AppScope
    fun provideHomeView(): HomeView {
        return view as HomeView
    }


    @Provides
    @AppScope
    fun provideHomeViewActivity(): HomeView.Activity {
        return view as HomeView.Activity
    }


    @Provides
    @AppScope
    fun provideHomePresenterActivity(presenter: HomeActivityPresenterImpl): HomePresenter.Activity {
        return presenter
    }


}