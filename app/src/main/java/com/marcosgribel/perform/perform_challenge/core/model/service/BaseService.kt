package com.marcosgribel.perform.perform_challenge.core.model.service

import io.reactivex.Observable

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface BaseService {

    fun getUrlApi(): String

}