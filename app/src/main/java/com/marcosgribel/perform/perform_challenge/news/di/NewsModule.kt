package com.marcosgribel.perform.perform_challenge.news.di

import com.marcosgribel.perform.perform_challenge.core.AppScope
import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView
import com.marcosgribel.perform.perform_challenge.news.presentation.presenter.NewsFragmentPresenterImpl
import com.marcosgribel.perform.perform_challenge.news.presentation.presenter.NewsPresenter
import com.marcosgribel.perform.perform_challenge.news.presentation.view.NewsView
import dagger.Module
import dagger.Provides

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class NewsModule(private val mView: BaseView) {

    @Provides
    @AppScope
    fun provideNewsView(): NewsView {
        return mView as NewsView
    }

    @Provides
    @AppScope
    fun provideNewsFragmentView(): NewsView.Fragment {
        return mView as NewsView.Fragment
    }


    @Provides
    @AppScope
    fun provideNewsFragmentPresenter(presenter: NewsFragmentPresenterImpl): NewsPresenter.Fragment {
        return presenter
    }

}