package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.ElementList

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */

data class ResultStable constructor(
        @field:Attribute(name = "type", required = false)
        var type: String? = null,
        @field:ElementList(inline = true)
        var ranks: List<Rank>? = null
)