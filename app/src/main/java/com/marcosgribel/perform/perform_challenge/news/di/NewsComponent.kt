package com.marcosgribel.perform.perform_challenge.news.di

import com.marcosgribel.perform.perform_challenge.core.AppScope
import com.marcosgribel.perform.perform_challenge.news.presentation.view.fragment.NewsListFragment
import dagger.Subcomponent

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@AppScope
@Subcomponent(modules = arrayOf(NewsModule::class))
interface NewsComponent {

    fun inject(fragment: NewsListFragment)

}