package com.marcosgribel.perform.perform_challenge.news.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.marcosgribel.perform.perform_challenge.R
import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Item
import com.marcosgribel.perform.perform_challenge.core.model.helper.DateHelper
import com.marcosgribel.perform.perform_challenge.core.presentation.adapter.BaseRecyclerViewAdapter
import com.marcosgribel.perform.perform_challenge.core.presentation.view.ui.BaseFragment


class NewsRecyclerViewAdapter constructor(val mContext: Context, val mOnListFragmentListener: BaseFragment.OnListFragmentInteractionListener?) : BaseRecyclerViewAdapter<Item>(mContext, mOnListFragmentListener) {

    inner class NewsViewHolder(private val mView: View) : BaseViewHolder(mView) {
        var imgCover: ImageView = mView.findViewById<ImageView>(R.id.img_cover_news)
        var txtTitle: TextView = mView.findViewById<TextView>(R.id.txt_title_news)
        var txtDescription: TextView = mView.findViewById<TextView>(R.id.txt_description_news)
        var txtPublishedDate: TextView = mView.findViewById<TextView>(R.id.txt_published_date_news)
    }

    var requestOptions = RequestOptions()

    init {

        requestOptions.override(100, 100)
        requestOptions.placeholder(R.drawable.ic_newspaper)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_fragment_news, parent, false)
        return NewsViewHolder(view)
    }


    override fun onBindViewHolder(holder: BaseViewHolder?, position: Int) {
        super.onBindView(holder, position)

        if (holder is NewsViewHolder) {
            val item = mItems[position]

            holder.txtTitle.text = item.title
            holder.txtDescription.text = item.description
            holder.txtPublishedDate.text = DateHelper.formartStringDate(item.pubDate, DateHelper.Pattern.EEE_dd_MMM_yyyy_hh_mm_ss_z.pattern, DateHelper.Pattern.EEEEE_dd_MMMMM_yyyy_hh_mm.pattern)


            Glide
                    .with(mContext)
                    .applyDefaultRequestOptions(requestOptions)
                    .load(item.enclosure?.url)
                    .into(holder.imgCover)

        }

    }

}
