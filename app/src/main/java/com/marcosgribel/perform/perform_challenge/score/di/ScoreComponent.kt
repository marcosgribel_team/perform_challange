package com.marcosgribel.perform.perform_challenge.score.di

import com.marcosgribel.perform.perform_challenge.core.AppScope
import com.marcosgribel.perform.perform_challenge.score.presentation.view.fragment.ScoreListFragment
import dagger.Subcomponent

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@AppScope
@Subcomponent(modules = arrayOf(ScoreModule::class))
interface ScoreComponent {

    fun inject(fragment: ScoreListFragment)

}