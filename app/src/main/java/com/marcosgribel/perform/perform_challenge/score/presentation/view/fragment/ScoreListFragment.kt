package com.marcosgribel.perform.perform_challenge.score.presentation.view.fragment


import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcosgribel.perform.perform_challenge.R
import com.marcosgribel.perform.perform_challenge.core.di.App
import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Competition
import com.marcosgribel.perform.perform_challenge.core.presentation.view.ui.BaseFragment
import com.marcosgribel.perform.perform_challenge.score.di.ScoreModule
import com.marcosgribel.perform.perform_challenge.score.presentation.adapter.ScoreRecyclerViewAdapter
import com.marcosgribel.perform.perform_challenge.score.presentation.presenter.ScorePresenter
import com.marcosgribel.perform.perform_challenge.score.presentation.view.ScoreView
import kotlinx.android.synthetic.main.fragment_score_list.*


/**
 * A simple [Fragment] subclass.
 */
class ScoreListFragment : BaseFragment<ScorePresenter.Fragment>(), ScoreView.Fragment {

    companion object {

        val TAG = ScoreListFragment::class.java.simpleName.toString()

        fun newInstance(): Fragment {
            return ScoreListFragment()
        }

    }

    lateinit var mAdapter: ScoreRecyclerViewAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.get(context)
                .getAppComponent()
                .plus(ScoreModule(this))
                .inject(this)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_score_list, container, false)

        mAdapter = ScoreRecyclerViewAdapter(context)

        var layoutManager = LinearLayoutManager(context)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view_of_scores)
        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, layoutManager.orientation))
        recyclerView.setHasFixedSize(true)

        recyclerView.adapter = mAdapter

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        basePresenter.loadData()
    }


    override fun setHeader(competition: Competition?) {
        if (competition == null) return

        txt_competition_name_score?.text = competition.name

    }

    override fun setContent(items: List<Any>?) {
        if (items == null) return

        mAdapter.clear()
        mAdapter.addAll(items)

        basePresenter.loadDataWithAutoRefresh()
    }


}// Required empty public constructor
