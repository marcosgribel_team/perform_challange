package com.marcosgribel.perform.perform_challenge.rank.model.di

import com.marcosgribel.perform.perform_challenge.core.AppScope
import com.marcosgribel.perform.perform_challenge.news.di.NewsModule
import com.marcosgribel.perform.perform_challenge.rank.presentation.view.ui.fragment.RankListFragment
import dagger.Subcomponent

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@AppScope
@Subcomponent(modules = arrayOf(RankModule::class))
interface RankComponent {

    fun inject(fragment: RankListFragment)

}
