package com.marcosgribel.perform.perform_challenge.news.presentation.view.fragment

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcosgribel.perform.perform_challenge.R
import com.marcosgribel.perform.perform_challenge.core.di.App
import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Item
import com.marcosgribel.perform.perform_challenge.core.presentation.view.ui.BaseFragment
import com.marcosgribel.perform.perform_challenge.news.di.NewsModule
import com.marcosgribel.perform.perform_challenge.news.presentation.adapter.NewsRecyclerViewAdapter
import com.marcosgribel.perform.perform_challenge.news.presentation.presenter.NewsPresenter
import com.marcosgribel.perform.perform_challenge.news.presentation.view.NewsView
import kotlinx.android.synthetic.main.fragment_news_list.*
import android.support.v7.widget.DividerItemDecoration




/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class NewsListFragment : BaseFragment<NewsPresenter.Fragment>(), NewsView.Fragment {


    companion object {
        val TAG = NewsListFragment::class.java.simpleName.toString()

        fun newInstance(): Fragment {
            return NewsListFragment()
        }

    }

    lateinit var mAdapter: NewsRecyclerViewAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        App.get(context)
                .getAppComponent()
                .plus(NewsModule(this))
                .inject(this)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_news_list, container, false)

        mAdapter = NewsRecyclerViewAdapter(context, mListFragmentInteractionListener)

        var layoutManager = LinearLayoutManager(context)

        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view_of_news)
        recyclerView.layoutManager = layoutManager
        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, layoutManager.orientation))
        recyclerView.setHasFixedSize(true)

        recyclerView.adapter = mAdapter

        return view

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        basePresenter.loadData()

    }


    override fun setHeader(strDate: String) {
        txt_published_date_news?.text = strDate
    }

    override fun setContent(items: List<Item>?) {
        if (items == null) return

        mAdapter.clear()
        mAdapter.addAll(items)

    }

}
