package com.marcosgribel.perform.perform_challenge.score.model.usecase

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Gsmrs
import com.marcosgribel.perform.perform_challenge.core.model.usecase.BaseUseCase
import io.reactivex.Observable

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface ScoreUseCase : BaseUseCase {

    fun get() : Observable<Gsmrs>

}