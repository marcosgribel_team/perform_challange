package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Root(strict = false)
data class Gsmrs constructor(
        @field:Attribute(name="version", required = false)
        var version: String? = null,
        @field:Attribute(name="sport", required = false)
        var sport: String? = null,
        @field:Element(name = "competition", required = false)
        var competition: Competition? = null
)