package com.marcosgribel.perform.perform_challenge.news.model.service

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Rss
import com.marcosgribel.perform.perform_challenge.core.model.service.BaseServiceImpl
import com.marcosgribel.perform.perform_challenge.core.model.service.util.BaseServiceConnector
import com.marcosgribel.perform.perform_challenge.core.model.service.util.RetrofitServiceConnector
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class NewsServiceImpl @Inject constructor(private val retrofit: RetrofitServiceConnector) : BaseServiceImpl(retrofit), NewsService {


    override fun getLatest(): Observable<Rss> {
        return retrofit.request(getUrlApi(), NewsServiceEndPoint::class.java).getLatest()
    }

}