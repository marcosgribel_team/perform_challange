package com.marcosgribel.perform.perform_challenge.news.presentation.view

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Item
import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface NewsView : BaseView {

    interface Fragment : NewsView {

        fun setHeader(strDate: String)

        fun setContent(items: List<Item>?)

    }
}