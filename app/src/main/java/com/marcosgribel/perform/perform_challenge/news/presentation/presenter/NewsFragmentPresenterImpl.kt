package com.marcosgribel.perform.perform_challenge.news.presentation.presenter

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Rss
import com.marcosgribel.perform.perform_challenge.core.model.helper.DateHelper
import com.marcosgribel.perform.perform_challenge.core.presentation.presenter.BasePresenterImpl
import com.marcosgribel.perform.perform_challenge.news.model.usecase.NewsUseCase
import com.marcosgribel.perform.perform_challenge.news.presentation.view.NewsView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class NewsFragmentPresenterImpl @Inject constructor(private val mView: NewsView.Fragment) : BasePresenterImpl(mView), NewsPresenter.Fragment {

    @Inject lateinit var useCase: NewsUseCase

    init {
        Timber.tag(NewsFragmentPresenterImpl::class.java.simpleName.toString())
    }


    override fun loadData() {

        mView.isToShowProgressBar(true)

        val disposable = useCase.getLatest()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mView.isToShowProgressBar(false)
                    setData(it)
                }, {
                    mView.isToShowProgressBar(false)
                    Timber.e(it)
                })

        mCompositeDisposable.add(disposable)
    }



    /**
     *   Set all data to display on view
     */
    private fun setData(rss: Rss?) {
        if (rss == null) return

        val strData = DateHelper.formartStringDate(rss.channel?.pubDate, DateHelper.Pattern.EEE_dd_MMM_yyyy_hh_mm_ss_z.pattern, DateHelper.Pattern.EEEEE_dd_MMMMM_yyyy_hh_mm.pattern)
        mView.setHeader(strData)

        mView.setContent(rss.channel?.item)

    }

}