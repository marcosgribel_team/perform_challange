package com.marcosgribel.perform.perform_challenge.core.presentation.view.ui

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.marcosgribel.perform.perform_challenge.core.presentation.presenter.BasePresenter
import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView
import kotlinx.android.synthetic.main.progress_bar.*
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
open class BaseFragment<V : BasePresenter> : Fragment(), BaseView.Fragment {


    @Inject lateinit var basePresenter: V


    interface OnListFragmentInteractionListener {

        fun <E> onListFragmentInteraction(item: E)

    }


    protected var mListFragmentInteractionListener: OnListFragmentInteractionListener? = null


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            mListFragmentInteractionListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListFragmentInteractionListener = null
    }

    override fun isToShowProgressBar(value: Boolean) {
        if(activity == null) return

        if (activity is AppCompatActivity) {
            (activity as AppCompatActivity).showProgressBar(value)
        }
    }
}