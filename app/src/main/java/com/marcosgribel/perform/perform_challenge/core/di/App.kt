package com.marcosgribel.perform.perform_challenge.core.di

import android.app.Application
import android.content.Context
import com.marcosgribel.perform.perform_challenge.core.di.component.AppComponent
import com.marcosgribel.perform.perform_challenge.core.di.component.DaggerAppComponent


import com.marcosgribel.perform.perform_challenge.core.di.module.AppModule
import timber.log.Timber

/**
 * Created by marcosgribel on 11/10/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class App : Application() {

    private lateinit var mAppComponent: AppComponent

    companion object {
        fun get(context: Context): App = context.applicationContext as App
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
        initTimber()
    }


    fun getAppComponent(): AppComponent = mAppComponent


    private fun initDagger() {
        mAppComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()
    }

    private fun initTimber(){
//        if(BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
//        }

    }
}