package com.marcosgribel.perform.perform_challenge.core.di.module

import com.marcosgribel.perform.perform_challenge.news.model.usecase.NewsUseCase
import com.marcosgribel.perform.perform_challenge.news.model.usecase.NewsUseCaseImpl
import com.marcosgribel.perform.perform_challenge.rank.model.usecase.RankUseCase
import com.marcosgribel.perform.perform_challenge.rank.model.usecase.RankUseCaseImpl
import com.marcosgribel.perform.perform_challenge.score.model.usecase.ScoreUseCase
import com.marcosgribel.perform.perform_challenge.score.model.usecase.ScoreUseCaseImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class UseCaseModule {

    @Provides
    @Singleton
    fun provideNewsUseCase(useCase: NewsUseCaseImpl): NewsUseCase {
        return useCase
    }

    @Provides
    @Singleton
    fun provideScoreUseCase(useCase: ScoreUseCaseImpl): ScoreUseCase {
        return useCase
    }

    @Provides
    @Singleton
    fun provideRankUseCase(useCase: RankUseCaseImpl) : RankUseCase {
        return useCase
    }

}