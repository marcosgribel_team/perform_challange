package com.marcosgribel.perform.perform_challenge.score.presentation.presenter

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Gsmrs
import com.marcosgribel.perform.perform_challenge.core.presentation.presenter.BasePresenterImpl
import com.marcosgribel.perform.perform_challenge.score.model.usecase.ScoreUseCase
import com.marcosgribel.perform.perform_challenge.score.presentation.view.ScoreView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class ScoreListFragmentPresenterImpl @Inject constructor(val mView: ScoreView.Fragment) : BasePresenterImpl(mView), ScorePresenter.Fragment {

    @Inject lateinit var useCase: ScoreUseCase

    init {
        Timber.tag(ScoreListFragmentPresenterImpl::class.java.simpleName.toString())
    }

    override fun loadData() {
        mView.isToShowProgressBar(true)
        val disposable = useCase.get()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mView.isToShowProgressBar(false)
                    setData(it)
                }, {
                    mView.isToShowProgressBar(false)
                    Timber.e(it)
                })

        mCompositeDisposable.add(disposable)
    }

    override fun loadDataWithAutoRefresh() {
        var disposable = Observable.just(true)
                .delay(30, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    loadData()
                }

        mCompositeDisposable.add(disposable)
    }

    private fun setData(gsmrs: Gsmrs){
        mView.setHeader(gsmrs.competition)

        val groups = gsmrs.competition?.season?.round?.groups
        var items: ArrayList<Any> = ArrayList()
        if(groups != null){
            for(g in groups){
                items.add(g)
                val matches = g.matches
                if(matches != null){
                    for(m in matches){
                        items.add(m)
                    }
                }
            }
        }

        mView.setContent(items)


//        mView.setContent(gsmrs.competition?.season?.round)
    }

}