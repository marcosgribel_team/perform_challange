package com.marcosgribel.perform.perform_challenge.rank.presentation.view

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Rank
import com.marcosgribel.perform.perform_challenge.core.presentation.view.BaseView

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface RankView : BaseView {

    interface Fragment : RankView {

        fun setContent(items: List<Rank>?)

    }

}