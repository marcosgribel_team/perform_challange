package com.marcosgribel.perform.perform_challenge.score.presentation.presenter

import com.marcosgribel.perform.perform_challenge.core.presentation.presenter.BasePresenter

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface ScorePresenter : BasePresenter {

    interface Fragment : ScorePresenter {

        fun loadData()

        fun loadDataWithAutoRefresh()

    }
}