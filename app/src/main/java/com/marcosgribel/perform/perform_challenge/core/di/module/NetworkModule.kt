package com.marcosgribel.perform.perform_challenge.core.di.module

import com.marcosgribel.perform.perform_challenge.BuildConfig
import com.marcosgribel.perform.perform_challenge.core.model.service.util.BaseServiceConnector
import com.marcosgribel.perform.perform_challenge.core.model.service.util.RetrofitServiceConnector
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import javax.inject.Singleton

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Module
class NetworkModule {

    @Provides
    @Reusable
    fun provideRetrofit(okhttp: OkHttpClient): Retrofit.Builder {
        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .client(okhttp)
    }

    @Provides
    @Reusable
    fun provideOkHttpBuilder(): OkHttpClient.Builder {
        return okhttp3.OkHttpClient.Builder()
    }

    @Provides
    @Reusable
    fun provideOkHttpClient(okHttpBuilder: OkHttpClient.Builder, loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        okHttpBuilder.addInterceptor(loggingInterceptor)
        return okHttpBuilder.build()
    }

    @Provides
    @Reusable
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return httpLoggingInterceptor
    }

    @Provides
    @Singleton
    fun provideBaseServiceConnector(retrofit: Retrofit.Builder, okHttpClient: OkHttpClient.Builder): BaseServiceConnector {
        return BaseServiceConnector(retrofit, okHttpClient)
    }

    @Provides
    @Singleton
    fun provideRetrofitServiceConnector(retrofit: Retrofit.Builder, okHttpClient: OkHttpClient.Builder): RetrofitServiceConnector {
        return RetrofitServiceConnector(retrofit, okHttpClient)
    }
}