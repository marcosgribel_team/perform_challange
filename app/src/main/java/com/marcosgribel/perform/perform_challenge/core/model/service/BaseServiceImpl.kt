package com.marcosgribel.perform.perform_challenge.core.model.service

import com.marcosgribel.perform.perform_challenge.BuildConfig
import com.marcosgribel.perform.perform_challenge.core.model.service.util.BaseServiceConnector
import com.marcosgribel.perform.perform_challenge.core.model.service.util.RetrofitServiceConnector
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
open class BaseServiceImpl @Inject constructor(val connector: BaseServiceConnector) : BaseService {


    override fun getUrlApi(): String {
        return BuildConfig.URL_API
    }


}
