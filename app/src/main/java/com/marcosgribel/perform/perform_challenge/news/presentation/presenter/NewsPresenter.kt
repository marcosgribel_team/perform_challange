package com.marcosgribel.perform.perform_challenge.news.presentation.presenter

import com.marcosgribel.perform.perform_challenge.core.presentation.presenter.BasePresenter

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface NewsPresenter : BasePresenter {

    interface Fragment : NewsPresenter {

        fun loadData()

    }
}