package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@Root(strict = false)
data class Competition constructor(
        @field:Attribute(name = "competition_id", required = false)
        var id: Long? = null,
        @field:Attribute(name = "name", required = false)
        var name: String? = null,
        @field:Attribute(name = "area_name", required = false)
        var areaName: String? = null,
        @field:Element(name = "season", required = false)
        var season: Season? = null
)