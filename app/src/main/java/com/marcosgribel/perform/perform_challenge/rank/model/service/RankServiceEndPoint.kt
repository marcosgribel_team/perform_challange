package com.marcosgribel.perform.perform_challenge.rank.model.service

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Gsmrs
import io.reactivex.Observable
import retrofit2.http.GET

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
interface RankServiceEndPoint {

    @GET("standings.xml")
    fun getStandings() : Observable<Gsmrs>

}