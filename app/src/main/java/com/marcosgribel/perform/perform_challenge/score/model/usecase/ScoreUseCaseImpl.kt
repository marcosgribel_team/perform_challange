package com.marcosgribel.perform.perform_challenge.score.model.usecase

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Gsmrs
import com.marcosgribel.perform.perform_challenge.core.model.usecase.BaseUseCaseImpl
import com.marcosgribel.perform.perform_challenge.score.model.service.ScoreService
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class ScoreUseCaseImpl @Inject constructor(val mService: ScoreService) : BaseUseCaseImpl(mService), ScoreUseCase {

    override fun get(): Observable<Gsmrs> {
        return mService.get()
    }

}