package com.marcosgribel.perform.perform_challenge.rank.presentation.presenter

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Gsmrs
import com.marcosgribel.perform.perform_challenge.core.presentation.presenter.BasePresenterImpl
import com.marcosgribel.perform.perform_challenge.rank.model.usecase.RankUseCase
import com.marcosgribel.perform.perform_challenge.rank.presentation.view.RankView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Timed
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class RankListFragmentPresenterImpl @Inject constructor(val mView: RankView.Fragment) : BasePresenterImpl(mView), RankPresenter.Fragment {

    @Inject lateinit var useCase: RankUseCase

    override fun loadData() {
        mView.isToShowProgressBar(true)

        var disposable = useCase.getStandings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mView.isToShowProgressBar(false)
                    setData(it)
                }, {
                    mView.isToShowProgressBar(false)
                    Timber.e(it)
                })

        mCompositeDisposable.add(disposable)
    }

    private fun setData(gsmrs: Gsmrs) {
        val listOfRank = gsmrs.competition?.season?.round?.resultStable?.ranks

        mView.setContent(listOfRank)
    }


}