package com.marcosgribel.perform.perform_challenge.core.di.module

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by marcosgribel on 11/10/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */

@Module(
        includes = arrayOf(
                BaseModule::class,
                UseCaseModule::class,
                ServiceModule::class,
                NetworkModule::class
        )
)
class AppModule constructor(val application: Application) {

    @Provides
    @Singleton
    fun provideApplication() = application

}