package com.marcosgribel.perform.perform_challenge.home.di.component

import com.marcosgribel.perform.perform_challenge.core.AppScope
import com.marcosgribel.perform.perform_challenge.home.di.module.HomeModule
import com.marcosgribel.perform.perform_challenge.home.presentation.view.ui.activity.HomeActivity
import dagger.Subcomponent
import javax.inject.Singleton

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
@AppScope
@Subcomponent(
        modules = arrayOf(HomeModule::class)
)
interface HomeComponent {

    fun inject(activity: HomeActivity)

}