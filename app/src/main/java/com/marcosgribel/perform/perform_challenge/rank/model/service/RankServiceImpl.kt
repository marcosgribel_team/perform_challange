package com.marcosgribel.perform.perform_challenge.rank.model.service

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Gsmrs
import com.marcosgribel.perform.perform_challenge.core.model.service.BaseServiceImpl
import com.marcosgribel.perform.perform_challenge.core.model.service.util.BaseServiceConnector
import com.marcosgribel.perform.perform_challenge.core.model.service.util.RetrofitServiceConnector
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class RankServiceImpl @Inject constructor(val mConnector: RetrofitServiceConnector) : BaseServiceImpl(mConnector), RankService {

    override fun getStandings(): Observable<Gsmrs> {
        return mConnector.request(getUrlApi(), RankServiceEndPoint::class.java).getStandings()
    }


}