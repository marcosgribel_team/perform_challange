package com.marcosgribel.perform.perform_challenge.score.model.service

import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Gsmrs
import com.marcosgribel.perform.perform_challenge.core.model.service.BaseServiceImpl
import com.marcosgribel.perform.perform_challenge.core.model.service.util.BaseServiceConnector
import com.marcosgribel.perform.perform_challenge.core.model.service.util.RetrofitServiceConnector
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class ScoreServiceImpl @Inject constructor(val retrofit: RetrofitServiceConnector) : BaseServiceImpl(retrofit), ScoreService {


    override fun get(): Observable<Gsmrs> {
        return retrofit.request(getUrlApi(), ScoreServiceEndPoint::class.java).get()
    }
}