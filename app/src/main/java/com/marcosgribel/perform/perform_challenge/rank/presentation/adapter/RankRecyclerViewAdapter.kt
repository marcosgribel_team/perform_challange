package com.marcosgribel.perform.perform_challenge.rank.presentation.adapter

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.marcosgribel.perform.perform_challenge.R
import com.marcosgribel.perform.perform_challenge.core.model.domain.entity.Rank
import com.marcosgribel.perform.perform_challenge.core.presentation.adapter.BaseRecyclerViewAdapter

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
class RankRecyclerViewAdapter constructor(private val mContext: Context) : BaseRecyclerViewAdapter<Rank>(mContext) {

    inner class RankRecyclerViewHolder(private val mView: View?) : BaseViewHolder(mView) {
        var txtPositionRank = mView?.findViewById<TextView>(R.id.txt_position_rank)
        var txtClubName = mView?.findViewById<TextView>(R.id.txt_club_name_rank)

    }

    override fun onBindViewHolder(holder: BaseViewHolder?, position: Int) {
        super.onBindView(holder, position)

        if (holder is RankRecyclerViewHolder) {
            var rank = mItems[position]


            if(position % 2 == 0) {
                holder.mBaseView?.setBackgroundColor(ContextCompat.getColor(mContext, R.color.divider))
            } else {
                holder.mBaseView?.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.white))
            }

            holder.txtPositionRank?.text = rank.rank.toString()
            holder.txtClubName?.text = rank.clubName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BaseViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_fragment_rank_list, parent, false)
        return RankRecyclerViewHolder(view)
    }

}