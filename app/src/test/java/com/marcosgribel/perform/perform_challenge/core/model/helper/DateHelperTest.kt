package com.marcosgribel.perform.perform_challenge.core.model.helper

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
class DateHelperTest {


    @Test
    fun test_date_format_EEE_dd_MMM_yyyy(){
        val strDate = "Tue, 01 Jan 2013 18:00:00 +0000"


        val str = DateHelper.formartStringDate(strDate, DateHelper.Pattern.EEE_dd_MMM_yyyy_hh_mm_ss_z.pattern, DateHelper.Pattern.EEEEE_dd_MMMMM_yyyy_hh_mm.pattern)
        assertNotNull(str)
        assertEquals("Tuesday, 01 January 2013, 04:00", str)
    }
}