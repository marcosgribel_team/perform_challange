package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.junit.Assert.*
import org.junit.Test
import org.simpleframework.xml.core.Persister

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
class ChannelParseXmlTest : BaseParseXmlTest() {


    @Test
    override fun parse_xml_with_success() {
        var serializer = Persister()
        val channel: Channel = serializer.read(Channel::class.java, getXml())

        assertNotNull(channel)
        assertNotNull(channel.pubDate)
        assertNotNull(channel.title)
        assertNotNull(channel.link)
        assertNotNull(channel.item)
        assertNotNull(channel.language)

    }

    override fun getXml(): String {
        return "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<channel>\n" +
                "<title>Perform Mobile Test - Latest News</title>\n" +
                "<description/>\n" +
                "<language>en-GB</language>\n" +
                "<pubDate>Tue, 01 Jan 2013 18:00:00 +0000</pubDate>\n" +
                "<link>\n" +
                "http://www.mobilefeeds.performgroup.com/interviews/technicaltest\n" +
                "</link>\n" +
                "<category>interview</category>\n" +
                "<category>Perform</category>\n" +
                "<item>\n" +
                "<guid>100001</guid>\n" +
                "<title>Lead story headline goes here</title>\n" +
                "<pubDate>Tue, 01 Jan 2013 17:00:00 +0000</pubDate>\n" +
                "<enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>\n" +
                "<description>Lead story teaser text here</description>\n" +
                "<link>\n" +
                "http://www.mobilefeeds.performgroup.com/utilities/interviews/techtest/webview/article.html/guid/100001\n" +
                "</link>\n" +
                "<category>News</category>\n" +
                "</item>\n" +
                "<item>\n" +
                "<guid>100002</guid>\n" +
                "<title>2nd story headline goes here</title>\n" +
                "<pubDate>Tue, 01 Jan 2013 16:00:00 +0000</pubDate>\n" +
                "<enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>\n" +
                "<description>2nd story teaser text here</description>\n" +
                "<link>\n" +
                "http://www.mobilefeeds.performgroup.com/utilities/interviews/techtest/webview/article.html/guid/100002\n" +
                "</link>\n" +
                "<category>News</category>\n" +
                "</item>\n" +
                "</channel>"
    }


}