package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.junit.Assert.*
import org.junit.Test
import org.simpleframework.xml.core.Persister

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
class GroupParseXmlTest : BaseParseXmlTest() {

    @Test
    override fun parse_xml_with_success() {
        var serializer = Persister()
        val group: Group = serializer.read(Group::class.java, getXml())

        assertNotNull(group)
        assertNotNull(group.id)
        assertNotNull(group.name)
        assertNotNull(group.matches)
    }

    override fun getXml(): String {
        return "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<group group_id=\"5227\" name=\"Group F\" details=\"\" winner=\"\" last_updated=\"2012-09-20 12:43:11\">\n" +
                "<match match_id=\"1388723\" date_utc=\"2012-09-19\" time_utc=\"18:45:00\" date_london=\"2012-09-19\" time_london=\"19:45:00\" team_A_id=\"895\" team_A_name=\"Lille\" team_A_country=\"FRA\" team_B_id=\"200\" team_B_name=\"BATE\" team_B_country=\"BLR\" status=\"Playing\" gameweek=\"1\" winner=\"\" fs_A=\"0\" fs_B=\"1\" hts_A=\"\" hts_B=\"\" ets_A=\"\" ets_B=\"\" ps_A=\"\" ps_B=\"\" last_updated=\"2012-09-19 22:56:46\"/>\n" +
                "<match match_id=\"1388724\" date_utc=\"2012-09-19\" time_utc=\"18:45:00\" date_london=\"2012-09-19\" time_london=\"19:45:00\" team_A_id=\"961\" team_A_name=\"Bayern München\" team_A_country=\"DEU\" team_B_id=\"2015\" team_B_name=\"Valencia\" team_B_country=\"ESP\" status=\"Playing\" gameweek=\"1\" winner=\"\" fs_A=\"2\" fs_B=\"2\" hts_A=\"\" hts_B=\"\" ets_A=\"\" ets_B=\"\" ps_A=\"\" ps_B=\"\" last_updated=\"2012-09-20 12:43:11\"/>\n" +
                "</group>"
    }
}