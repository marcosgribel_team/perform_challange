package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.junit.Assert.*
import org.junit.Test
import org.simpleframework.xml.core.Persister

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
class MatchParseXmlTest : BaseParseXmlTest() {


    @Test
    override fun parse_xml_with_success() {
        var serializer = Persister()
        val match: Match = serializer.read(Match::class.java, getXml())

        assertNotNull(match)
        assertNotNull(match.id)
        assertNotNull(match.teamAName)
        assertNotNull(match.teamBName)
        assertNotNull(match.fsA)
        assertNotNull(match.fsB)
    }

    override fun getXml(): String {
        return "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<match match_id=\"1388664\" date_utc=\"2012-09-19\" time_utc=\"18:45:00\" date_london=\"2012-09-19\" time_london=\"19:45:00\" team_A_id=\"662\" team_A_name=\"Manchester United\" team_A_country=\"ENG\" team_B_id=\"2217\" team_B_name=\"Galatasaray\" team_B_country=\"TUR\" status=\"Playing\" gameweek=\"1\" winner=\"\" fs_A=\"0\" fs_B=\"1\" hts_A=\"\" hts_B=\"\" ets_A=\"\" ets_B=\"\" ps_A=\"\" ps_B=\"\" last_updated=\"2012-09-19 22:53:37\"/>"
    }


}