package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.junit.Assert.*
import org.junit.Test
import org.simpleframework.xml.core.Persister

/**
 * Created by marcosgribel on 11/12/17.
 *
 *
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
class RankParseXmlTest : BaseParseXmlTest(){

    @Test

    override fun parse_xml_with_success() {
        var serializer = Persister()
        val rank: Rank = serializer.read(Rank::class.java, getXml())

        assertNotNull(rank)
        assertNotNull(rank.areaId)
        assertNotNull(rank.clubName)
        assertNotNull(rank.countryCode)
        assertNotNull(rank.poits)
        assertNotNull(rank.goalsAgainst)

    }

    override fun getXml(): String {
        return "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<ranking rank=\"1\" last_rank=\"1\" zone_start=\"CL\" team_id=\"676\" club_name=\"Manchester City\" countrycode=\"ENG\" area_id=\"68\" matches_total=\"38\" matches_won=\"28\" matches_draw=\"5\" matches_lost=\"5\" goals_pro=\"93\" goals_against=\"29\" points=\"89\"/>"
    }
}