package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.junit.Assert.*
import org.junit.Test
import org.simpleframework.xml.core.Persister

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
class EnclosurePaserXmlTest : BaseParseXmlTest() {


    @Test
    override fun parse_xml_with_success() {
        var serializer = Persister()
        val enclosure: Enclosure = serializer.read(Enclosure::class.java, getXml())


        assertNotNull(enclosure)
    }


    override fun getXml(): String {

        return "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>"
    }
}