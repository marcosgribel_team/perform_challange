package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.junit.Test

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 *
 */
abstract class BaseParseXmlTest {

    @Test
    abstract fun parse_xml_with_success()

    abstract fun getXml() : String
}