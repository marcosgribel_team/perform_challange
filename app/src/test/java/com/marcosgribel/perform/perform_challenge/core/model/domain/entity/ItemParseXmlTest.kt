package com.marcosgribel.perform.perform_challenge.core.model.domain.entity

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.simpleframework.xml.core.Persister

/**
 * Created by marcosgribel on 11/11/17.
 *
 *
 *
 *
 * Copyright 2017 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel - gribel.marcos@gmail.com
 */
class ItemParseXmlTest : BaseParseXmlTest() {


    @Test
    override fun parse_xml_with_success() {
        var serializer = Persister()
        val item: Item = serializer.read(Item::class.java, getXml())

        assertNotNull(item)
        assertNotNull(item.guid)
        assertNotNull(item.category)
        assertNotNull(item.description)
        assertNotNull(item.enclosure)

    }


    override fun getXml(): String {
        return "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<item>\n" +
                "<guid>100001</guid>\n" +
                "<title>Lead story headline goes here</title>\n" +
                "<pubDate>Tue, 01 Jan 2013 17:00:00 +0000</pubDate>\n" +
                "<enclosure length=\"9048\" url=\"http://www.mobilefeeds.performgroup.com/javaImages/4b/ce/0,,14012~11849291,00.jpg\" type=\"image/jpeg\"/>\n" +
                "<description>Lead story teaser text here</description>\n" +
                "<link>\n" +
                "http://www.mobilefeeds.performgroup.com/utilities/interviews/techtest/webview/article.html/guid/100001\n" +
                "</link>\n" +
                "<category>News</category>\n" +
                "</item>"
    }
}