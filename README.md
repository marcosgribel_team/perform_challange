# README #


### Get Started ###

* [Android Studio](https://developer.android.com/studio/index.html)
* [Java 1.8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
* [Configurar o plugin do Kotlin no Android Studio](https://blog.jetbrains.com/kotlin/2013/08/working-with-kotlin-in-android-studio/)


### Project Structure

- The project is based on [MVP](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter) and [Clean Archictecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html).

- All packages is organized by feature, following this pattern to provide a better organization and easy maintenance.

- The project is structured using product flavors, divided and Prod and Mock. 
#### Flavors 
[Read More](https://developer.android.com/studio/build/build-variants.html?)

#####`Prod` 
it will be deployed in Google Play store, this flavor is integrated with online API.

#####`Mock` 
it's to simulate API integration and useful when you are building project in the same time that back-end team are developing API. With this way is possible simulate all API integration and easy mock values. 

### Considerations

Well, I decided to use MVP and Clean Architecture to build this project with intention to ilustrate a real scenario about a new project.

This way, following this architectures patterns is great to create a project that will be `testable`, 
`scalable` and `easy to maintenance`. 

Android devices has a lot resources limitation, mainly memory and because this I decide to use Dagger to help in dependency Injection and apply some projects patterns, mainly Singleton to avoid creation unnecessary instance for some class.


### Libraries 

* Retrofit 2.x
* Rx Android 2.x
* Okhttp
* Glide
* Custom Tabs
* Timber
* Shimmer
 

### Author
* Marcos Gribel
* gribel.marcos@gmail.com